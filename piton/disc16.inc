;LBA = ( (cylinder * heads_per_cylinder + heads ) * sectors_per_track ) + sector - 1
;MBR:
;00h 1 ������� ���������� �������
;01h 1 ������ ������� - �������
;02h 1 ������ ������� - ������ (���� 0-5), ������� (���� 6,7)
;03h 1 ������ ������� - ������� (������� ���� 8,9 �������� � ����� ������ �������)
;04h 1 ��� ���� �������
;05h 1 ����� ������� - �������
;06h 1 ����� ������� - ������ (���� 0-5), ������� (���� 6,7)
;07h 1 ����� ������� - ������� (������� ���� 8,9 �������� � ����� ������ �������)
;08h 4 �������� ������� �������
;0Ch 4 ���������� �������� �������	
;������ ����:
;0-79 �������
;0-1 �������
;1-18 �������

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                              ReadDisc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;ReadDisc - read required nember sectors from disc
;input:
;dh - number of head
;cl - start sector
;ch - low part number of cilinder
;ax - sectors for read
;bx - start segment to read
;dl - disc
;output:
;ax: 1 -error, 0 -success
;������ ��� ��������� �� ��������� ������� �������
bnd dw PROGRAM_SEGM + 1000h
ReadDisc:
		push	es
		mov		bp, ax	
_nextReadDisc:			
		xor		ax, ax
		mov		al, 19
		sub		al, cl
		
		cmp		ax, bp
		jle		@f
		mov		ax, bp
		
@@:
		
		mov		es, bx
		;prevention attempted DMA across 64K boundary error 
		shl		ax, 5
		add		bx, ax
		sub		bx, [bnd]
		jb		_cnr1		
		sub		ax, bx			
		mov		bx, [bnd]
		add		bx, 1000h
		mov		[bnd], bx
_cnr1:
		shr		ax, 5				
		push	es
		xor		bx, bx
		mov		ah, 2;����� �������		
		int     13h	;���������� ���� ���. � ������	
			
		jc		discError ;���� ������ �F ����������
		
		
		pop		bx		
		xor		ah, ah
		sub		bp, ax
		add		cl, al
		cmp		cl, 19
		jl		@f
		mov		cl, 1		
		xor		dh, 1
		test	dh, dh
		jnz		@f
		inc		ch
@@:		
		shl		ax, 5		
		add		bx, ax
		test	bp, bp
		jnz		_nextReadDisc
		


		xor		ax, ax
		jmp		@f
discError:
		add		sp, 2
@@:
		
		pop		es	
		ret


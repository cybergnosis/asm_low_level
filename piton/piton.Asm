;Game PITON
;Classic type
;World of 70 previus centure today
;Main file of source code
;Recuire Disk16.inc & font8x16.inc
;For compilation use flat assembler - fasm
;subrealist@yandex.ru

include	'c:\fasm\include\macro\struct.inc'		;����������� ������� ��������												

MENU_COMMAND = 1			;��������� ���� � ����� ����   - ESC
HELP_COMMAND = 3Bh 			;������ �������				   - F1 
GAME_COMMAND = 3Ch			;����� ����					   - F2
CONT_COMMAND = 3Dh			;���������� ����			   - F3
RESET_COMMAND = 3Eh			;������������ ����������       - F4 
OFF_COMMAND = 3Fh			;���������� ����������         - F5
LOAD_ADDRESS = 7C00h		;����� �������� ������������ ����
UP_ARROW = 48h				;�������� �����
LEFT_ARROW = 4Bh			;�������� �����
RIGHT_ARROW = 4Dh			;�������� ������
DOWN_ARROW = 50h			;�������� ����
TRAIL_START = 4000h
RABBITS_START = 2000h
MAX_SCREEN_X	= 79		;����������� ���������� ������ �� X
MAX_SCREEN_Y	= 24		;����������� ���������� ������ �� Y
MAX_X 	= MAX_SCREEN_X - 2	;������������ ������� �� ��� X
MAX_Y 	= MAX_SCREEN_Y - 3	;������������ ������� �� ��� Y
LDR_MSG_CLR =111b			;
RM_OS_CLR = 001b
PM_OS_CLR = 010b
PROGRAM_SEGM = 1000h		;������� ���� ���������
RABBIT_NUM = 23				;����� ��������
FIELDBG = 0100000b
PITCL = 0100b
RABBITCL = 1111b
BOUNDCL = 1011b
HDRGAMECL = 111110b
FRAMEBUFFER = 0B800h
PRINT_COLOR = 7				;���� ��� ������ ���� - ����� ���� �� ������ ����
IS_GAME = 0FFh				;�������� �������� ���� ��� ����������� ����������
IS_MENU	= 0 				;�������� �������� ���� ��� ����������� ����������
TIMER_COUNTER equ [ss:46Ch]	;[00h:46Ch] - ����� ����������
							;��������� ���� �������� ����� 
							;��������� �������,
							;��������������� � ����� 
							;������ �������
KEYBOARD_INT equ ss:4*9	;[00h:4*9]������ ���������� ����������
RABBITS_DATA_REG equ fs	;������� ��� ������ �������
KILLED_RABBIT_SIGNATURA_X equ 0FFh
PYTHON_HEAD_SYMBOL equ 2
PYTHON_TRAIL_SYMBOL equ '@'
RABBIT_SYMBOL equ '&'

struct POINT
    x db 0
    y db 0
ends

;�������� ������
format binary
;������������ 16-�� ������ ���
USE16
org LOAD_ADDRESS			
_start:		
;������������� ���������� �������� � ��������� ����� � 0	
	xor		ax, ax
	mov		ss, ax
	mov		es, ax
	mov		ds, ax		
	mov		ss, ax			
	mov		sp, ax
	push	dx			;����� ������������ ����������, ��������� ����
	mov		ax,	0003h	;�������� �����, ���������� ��������� ����������
						;80�25, 16 ������ ��� ��������, 8 ��� ����
	int		10h			;����������� ����������� BIOS
						;������� ������������ ���������� � AH
						;��������� ���������� � ������ �����				
	mov		ah, 13h		;����� ��������� �� �����
	xor		al, al		;������������ ������� � BL, �� ������� ������	
	mov		cx, start_msg_l	;����� ���������
	mov		bl, LDR_MSG_CLR	;������� �����	
	xor		dx, dx		;����� ������, ����� �������
	mov		bp, start_msg	;[es:bp] ����� ���������
	int		10h			;������� ��������� ������
;����������� � ����� � ������ �������� ���������	
	mov		bx, PROGRAM_SEGM	;���� ����������		
	mov		es, bx				;���������� �����
	pop		dx					;����� ������������ ����������
	mov		dh,0 				;����� �������
	mov		ax, @end_prog/512	;�������� ��� ������		
	mov		cl, 2				;����� �������
	mov		ch, 0				;������� ����� ������ ��������	
	call	ReadDisk			;�������� � �����
	test	ax, ax				;��������� �� ������ �����������
	jnz		DiskReadError		;���� ������ - � ����������� ������
;������� ��������� � �������� �������� ��������� � ������
	mov		bx, PROGRAM_SEGM	
	mov		es, bx
	mov		ds, bx		
	xor		bx, bx
	mov		ah, 13h		
	xor		al, al			
	mov		cx, copy_msg_l	
	mov		bl, LDR_MSG_CLR		
	xor		dx, dx
	inc		dh					;�������� �� ������ ���� �����������
	mov		bp, copy_msg
	int		10h	
;��������� � ���������� �������� ���������
	jmp		PROGRAM_SEGM:@start_program	;� ������� �� ���� ����������
;���������� ������ �������� ���������
DiskReadError:
		xor		bx, bx
		mov		es, bx
		mov		si, ErrCode		
		call	dw2ah		;����������� ��� ������ � �������
		xor		bx, bx
		mov		ah, 13h		;�������	������� �� �����		
		xor		al, al		;������ �������� ������ �������, ������������ ������ � ����� ������
		mov		cx, DiskReadErrorMsg_l	;����� ��������
		mov		bl, LDR_MSG_CLR		;��������
		xor		dx, dx			;������/�������
		inc		dh
		mov		bp, DiskReadErrorMsg	;������ ��������
		int		10h		;�������
		hlt				;���������� ���������
start_msg db "Loader started"
start_msg_l = $ -  start_msg	
DiskReadErrorMsg db "Disk read error: "
ErrCode	db 9 dup 0
DiskReadErrorMsg_l = $ - DiskReadErrorMsg 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                            dw2ah
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; dw2ah - ����������� ������� ����� � ANSII �������������
;����:
;eax - ������ ��� ��������������
;si - ����� ��� ������ (size of data + 1 byte for 'h')
dw2ah:	
	mov		bx, si		;��������� �� ������ ������
	add		bx, 8		;��������� �� ����� ������
	mov		cl, 'h'		
	mov		[bx], cl	;�������� ������ 'h'
;�����������
@convert:
	dec		bx			;��������� ��������� ��� �������
						;���������� ���������������� ���������
	mov		edx, eax	;���������� ������ ��� ��������������
	and		edx, 0Fh	;��������� ������� ��������
	cmp		edx, 10		;���������� � 10
	jae		@ae10		;���� ������ ��� �����, ��������� � ���������
						;��� �������� ������������� ������
	add		edx, '0'	;����� ��������� �������� ������� "0"
	jmp		@to_buf		;� � ����������� � �����
@ae10:
	sub		edx, 10		;���� ������ 10 �������� 10
	add		edx, 'A'	;� ��������� �������� ������� "A"
@to_buf:
	mov		[bx], dl	;����������� � �����
	ror		eax, 4		;��������� �������� ��� ��������������
	cmp		bx, si		;���� ������ ����������� ������ �� ����������
	jg		@convert	;�� � �������������� ���������� ���������
	ret	
	
;LBA = ( (cylinder * heads_per_cylinder + heads ) * sectors_per_track ) + sector - 1
;������ ����:
;0-79 �������
;0-1 �������
;1-18 �������
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                              ReadDisk
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;ReadDisk - ������ ����������� ����� �������� � �����
;���������� ������ BIOS
;����:
;dh - ����� �������
;cl - ��������� ������
;ch - ������� ����� ������ ��������
;ax - �������� ��� ������
;bx - ��������� ���������� ����� ��� ����������� � �����
;dl - ����� ������������ ����������
;�����:
;ax: 0 - �������, 1 - ������
;������ ��� ��������� �� ��������� ������� �������
FIRST_SECTOR_NEXT_TRACK = 19 ;������ ������ �� ��������� �������
boundary dw PROGRAM_SEGM + 01000h
ReadDisk:
	push	es			;��������� �������� ES
	mov		bp, ax		;����� �������� ��� ������
_nextReadDisk:			
	xor		ax, ax		;��������
	mov		al, FIRST_SECTOR_NEXT_TRACK ;������ �� ��������� �������
						;����� ���� ��������� ������ ������� �� �������
						;������� ���������� � 1
	sub		al, cl		;����������� ���������� ����� �������� ��� ������
	cmp		ax, bp		;���������� � ����� ����� �������� ��� ������
	jle		@f			;���� �������� ��������� ������ ��� ���������
	mov		ax, bp		;�������� �� ����� ����������
@@:		
	mov		es, bx		;���������� ����� ������ ��� �����������
;�������������� ������ ����������� ������� 64K ��� DMA �������
	shl		ax, 5		;����� �� ���������� ������� 64�?
						;�������� ����� �������� ��������������� � 
						;���������� ����� ������
	add		bx, ax		;�������� ���������� ����� �����������
	sub		bx, [boundary] ;���������� � ��������
	jb		_cnr1		;���� �� ����� ��������� � �����������			
	sub		ax, bx		;���� �����, �������� ������� �� �����������
						;�������
	mov		bx, [boundary] ;� ����� �������� �������
	add		bx, 1000h
	mov		[boundary], bx ;���������� �������� ����� ������� 64�
_cnr1:
	shr		ax, 5		;���������� ����� ����� � ����� ��������		
	push	es			;���������� ����� ������ ��� �����������
	xor		bx, bx		;�������� ������ ������������ ES
	mov		ah, 2		;����� �������		
	int     13h			;���������� ���� ���. � ������				
	jc		DiskError 	;���� ������ �F ����������		
	pop		bx			;���������� ����� ������ ��� �����������
	xor		ah, ah		;����� �������� ��� ������
	sub		bp, ax		;���������� ����� �������� ��� ������
	add		cl, al		;��������� �� ����� �������
	cmp		cl, FIRST_SECTOR_NEXT_TRACK
	jl		@f			;���� �� ���������
						;� ���������� ���������� ����� ������
	mov		cl, 1		;���� ���������, ������ ������� ��������� �������
	xor		dh, 1		;��������� �������
	test	dh, dh		;������� �� �������?
	jnz		@f			;��������� ������� ��� ���������, �����
	inc		ch			;��������� �������
@@:		
	shl		ax, 5		;����������� ���������� ����� ������ ��� ���������� 						
	add		bx, ax		;�����������
	test	bp, bp		;���� �������� �� ������������� ������� 
	jnz		_nextReadDisk ;- ����������
	xor		ax, ax		;��� ������
	jmp		@f			;� ���������� �������
DiskError:
	add		sp, 2		;������� ����
@@:		
	pop		es			;���������������
	ret					;������������
;����� ������ ������� �������
db   200h-$+LOAD_ADDRESS-2 dup 0	;���� �� ���������
db 55h, 0AAh		;��������� ������������ ����������

;�������� ���������
org 0
__start_copy:
copy_msg  		db "The program was loaded successfully!" ;��������� � ��������	
														  ;��������
copy_msg_l = $-copy_msg
@start_program:
;������������� ���������� �������� � ����
	xor		ax, ax
	mov		ss, ax		;���� �� ������� ������ 64�
	mov		sp, ax		;� 0, ����� ������ ��������� �������� �����
						;��������� � ��������� ������������
						;��� ������ ����������� � ���� ��������
						;�������� �� ��� �������
	push	cs			;������� ����
	pop		ds			;����� �������� ������
	mov		ax, FRAMEBUFFER ;����������
	mov		es, ax
	mov		ax, TRAIL_START	;���������� ������ ������
	mov		gs, ax			;���������� �����
	mov		ax, RABBITS_START ;���������� ��������
	mov		RABBITS_DATA_REG, ax ;���������� �����
;������������� ���������� ���������� ����������	
	cli					;��������� ����������
	mov		word[KEYBOARD_INT], GameKeyHandler	;�������� ��� IP
	mov		word[KEYBOARD_INT+2], PROGRAM_SEGM	;����� ���������� ����� ��� CS	
	sti										;��������� ����������
	call	PrintMenu		;������� ���� ����� ������						
_menuLoop:
	jmp		$				;����� � ����������� ����
;������� ���� ���������
_mainLoop:
	mov		al, TIMER_COUNTER	;�������� ������� ���� ���������� �������
	cmp		al, [tick]			;���������� � ����������� ���������
	jz		_checSec			;���� �����, ��������� � �������� ������
	push	ax					;�����, ��������� �������� ���������� �������
	mov		si, szTail+6		;����� ������ ������
	xor		ax, ax
	mov		di, MAX_SCREEN_X - 12	;������� ��� ������
	call	print				;������� �������� ����� ������ ������
	pop		ax					;��������� ������� ���� ��������
	mov		[tick], al			;���������� �������
	and		al, 11b				;��������� �� �������� �������� �����
								;���������� ������� ������ ��������� 2 ����
	cmp		al, 11b				;� ���������� � 3
	jnz		_checSec			;���� �� �����, ��������� �������
	mov		al, [direction]		;����� �������� �����������
	call	MovePiton			;� ���������� ������
;������ ����� ������
_checSec:
	xor		al, al
	out		70h, al				;������ ������� ��� ������
	in		al, 71h				;������
	cmp		al, [savedSec]		;���������� � ����������� ���������
	je		_nonMoveRabbit		;���� ����� - �� ���������� ��������
	mov		[savedSec], al		;����� - ��������� ����� �������� ������
	inc		[time]				;����������� �������� ������ ������� ������ ���������
	mov		ax, [time]
	mov		bx, 3600			;�� �������� ������ ������ ���������
	xor		dx, dx
	div		bx					
	mov		di, szTime			;�������� ����
	mov		si, 2
	call	ui2a				;�������� ����������� � ����������� ���������� �����
	mov		ax, dx
	xor		dx, dx	
	mov		bx, 60				
	div		bx					
	add		di,  3				
	call	ui2a				;������		
	mov		ax, dx
	add		di, 3
	call	ui2a				;�������
	mov		di, MAX_SCREEN_X - 7
	mov		si, szTime
	xor		ax, ax
	call	print				;������� �������� �������
	call	BornRabbit			;�������� ���� ���������
	xor		si, si
	mov		cx, RABBIT_NUM	
	rdtsc						;�������� �������� �������� ������ ����������
								;������ �������� ������������ ��� ��������
								;��������������� �����, ��� �����������
								;����������� ����������� ��������
;���� ����������� ��������
@@:	
	push	ax
	rdtsc
	pop		bp
	add		ax, bp	;���������� ����� � ������ �������� ������� ����� TSC
	mov		bl, [RABBITS_DATA_REG:si+POINT.x]
	cmp		bl, KILLED_RABBIT_SIGNATURA_X	;������ ���������?
	jz		_prepNewMovRubbit			;���� ���, ��������� �� ����������� ����������
	mov		[rabbit_tmp_pos.x], bl		;��������� ���������� ������� �� ����. ����������
	mov		bh, [RABBITS_DATA_REG:si+POINT.y]
	mov		[rabbit_tmp_pos.y], bh
	and		al, 11	;��������� ��������� ��� ���� ����. ����� TSC
	cmp		al, 1	;���������� � 1, �������� �� ����������� ���������� X
	jnz		_checkForDecX	;���� �� �����, ��������� �� ����������� ���������� X
	inc		bl				;����� ����������� X
	mov		[rabbit_tmp_pos.x], bl	;��������� ����������� X �� ��������� ����������
	jmp		_checkForChangeY	;��������� � �������� �� ����������� ��������� Y
_checkForDecX:
	cmp		al, 2				;���� �� ����� 2
	jnz		_checkForChangeY	;��������� � �������� �� ����������� ��������� Y
	dec		bl					;����� ��������� X
	mov		[rabbit_tmp_pos.x], bl	;��������� ���������� �������� X	 
_checkForChangeY:		;����������� �� �� ��������� �������� �� ����������� ���������
						;� ��������� ��� Y ��� ���� ��� X
	push	ax
	rdtsc
	pop		bp
	add		ax, bp		
	mov		bl, [RABBITS_DATA_REG:si+POINT.y]
	and		al, 11
	cmp		al, 1
	jne		_checkForDecY
	inc		bl
	mov		[rabbit_tmp_pos.y], bl
	jmp		_moveRabbitCont
_checkForDecY:
	cmp		al, 2
	jne		_moveRabbitCont
	dec		bl
	mov		[rabbit_tmp_pos.y], bl	;��������� ���������� �������� Y
_moveRabbitCont:
	movzx	bx, [rabbit_tmp_pos.x]	;���������� � �������� ������� ������� 
	movzx	ax, [rabbit_tmp_pos.y]	;�� ��������� ����������
	cmp		bx, MAX_X
	ja 		_prepNewMovRubbit		;���� ������� ������ �����������
	cmp		bx, 0					;��� ������ �����������
	jl		_prepNewMovRubbit		;�� ��������� � ����������� ����������
	cmp		ax, MAX_Y				;�������, �������� ������� ��������
	ja 		_prepNewMovRubbit		;��� ���������
	test	ax, 0
	jl		_prepNewMovRubbit
	call	GetPosContent			;�����, �������� ������ �� ��������� ���������� �������
	cmp		al, PYTHON_TRAIL_SYMBOL	;���� �� ����� ������ ������
	jz		_prepNewMovRubbit
	cmp		al, RABBIT_SYMBOL		;��� ��� ������ ������
	jz		_prepNewMovRubbit		
	cmp		al, PYTHON_HEAD_SYMBOL	;��� ������ ������
	jz		_prepNewMovRubbit		;��������� � ���������� ����������� ������� �������
	movzx	bp, byte[RABBITS_DATA_REG:si+POINT.x]	;����� ������� ������� �� ������
	movzx	ax, byte[RABBITS_DATA_REG:si+POINT.y]	;������� �� ������
	call	UnSetRabbit
	mov		al, [rabbit_tmp_pos.x]					;������������� ������ � ����� �������
	mov		byte[RABBITS_DATA_REG:si+POINT.x], al	;������� � ��� ������ ��
	mov		al, [rabbit_tmp_pos.y]					;�� ��������� �������
	mov		byte[RABBITS_DATA_REG:si+POINT.y], al
	movzx	bp, byte[RABBITS_DATA_REG:si+POINT.x]	;� ������������� ����� �������	
	movzx	ax, byte[RABBITS_DATA_REG:si+POINT.y]	;������� �� ������
	call	SetRabbit
_prepNewMovRubbit:					;���������� � �����������
	add		si, sizeof.POINT   		;���������� �������
	dec		cx						;��������� ������� ��������								
	jz		@f						;���� ������� ���� �������� �����������
									;������� �� �����
	jmp		@b						;����� ������������ ������� ����������		
@@:	

_nonMoveRabbit:						;�� ���������� �������
	jmp		_mainLoop				;�������� ����
;���������� ������������ � ���� ������� ����������
GameKeyHandler:
	pushad							;��������� ��� ��������
	xor		dx, dx					;���� ��� ����������� �������������
	in		al, 61h					;�������� 
	mov		[keyState], al			;� ��������� ��������� ����������
	or		al, 80h					;��������� ����������
	out		61h, al					;
	xor		ax, ax					
	in		al, 60h					;�������� ������� ��������� ������� �������
;������������ ���������� �������
	cmp		al, MENU_COMMAND		;ESC - ��������� ���� � ����� ����
	jnz		_right					;����� ��������� �������� ������
	mov		[printColor], PRINT_COLOR ;������������� ���� ������
	call	PrintMenu				;������� ����
	cmp		[inGame], IS_MENU		;���������, ����� �� ���� ��� ����
	jz		_contMenuCommand		;���� ��� �  ���� - �� ������ ������, ��������� ����� ������� �������
	popad							;����� ������ ���������� ��������� ��������� ���� �� ����� ���������� � ����
	mov		[saveEAX], eax
	mov		[saveEBX], ebx
	mov		[saveECX], ecx
	mov		[saveEDX], edx
	mov		[saveESI], esi
	mov		[saveEDI], edi
	mov		[saveEBP], ebp
	pop		ax						;�������� IP �������� �� �����, �������� ���� ��� ��������
	mov		[saveIP], ax			;����� ������� ����������� ����������
	pop		ax						
	push	PROGRAM_SEGM			;�������� ����� �� ��������� ����� ����
	push	_menuLoop
	pushad							;����� ��������� �������� � ����
	mov		[inGame], IS_MENU		;������������� �������, ��� �� � ����
_contMenuCommand:
	jmp		_goOldKeyHandler		;� ������ �� �����������
_right:	
	cmp		al, RIGHT_ARROW			;�������� ������
	jnz		_left					;����� ��������� �������� �����	
	mov		[direction], al			;�������� � ���������� �����������
	jmp		_goOldKeyHandler
_left:
	cmp		al, LEFT_ARROW			;�������� �����
	jnz		_up						;����� ��������� �������� �����
	mov		[direction], al	
	jmp		_goOldKeyHandler
_up:
	cmp		al, UP_ARROW			;�������� �����
	jnz		_down					;����� ��������� �������� ����
	mov		[direction], al
	jmp		_goOldKeyHandler
_down:
	
	cmp		al, DOWN_ARROW			;�������� ����
	jnz		_gameCommand			;����� ��������� - ������ ����� ����
	mov		[direction], al	
	jmp		_goOldKeyHandler
_gameCommand:	
	cmp		al, GAME_COMMAND		;������ ����� ����
	jnz		_helpCommand			;����� ��������� ����� �������
;������ ����� ����
_gameCommand1:
	popad							;��������������� ��������
	add		sp, 2					;������������� ����� ����� ��������
	push	_mainLoop
	pushad							;��������� �������� �����
	mov		[isGame], IS_GAME		;������������� ������� ����
	mov		[time], 0				;����� � 0
	mov		dword[szTail+6], dword '0000' ;����� ������ � 0 (������������ ��������)
	xor		ax, ax					;������� ������ � ����� ������� ���� ������
	mov		[pit_pos_x], ax
	mov		[pit_pos_y], ax
	mov		[pit_length], ax		;����� ������ � 0
	mov		[direction], RIGHT_ARROW	;����� �������� ������
	xor		di, di						;������ ������ � ��������
	mov		bx, MAX_X					;������� ������ ��� �����������
	mov		si, MAX_Y
	mov		cx, RABBIT_NUM				;����� ��������
	rdtsc								;��� �������� ��������������� ����� ��� ��������� ��������
;���� ��������� ���������
@@:
;���� ���������� ��������������� ��������� ��������
	push 	ax							;������� �������� �������� ����� TSC � ����
	rdtsc	
	pop		bp	
	add		ax, bp						;�������� ���������� � ����������� �������� �������� ����� TSC
	xor		dx, dx 	
	div									;����� �� �������� �� �	
	mov		[fs:di+POINT.x], dl			;������� ��� ����������
	xor		dx, dx
	push	ax
	rdtsc
	pop		bp							;�� �� ��� Y ��� ���� ��� X
	add		ax, bp
	xor		dx, dx		
	div		si							;����� �� �������� �� Y
	mov		[fs:di+POINT.y], dl		    ;������� ��� ���������� Y	
	add		di, sizeof.POINT			;� ����������� ���������� �������
	loop	@b							;� ������ ����� ����������� ���������
;��������� �������� �� ������
	xor		bx, bx						;�������� �� ������ ������ ��������� ��������
	mov		cx, RABBIT_NUM
@@:	
	movzx	bp, byte[fs:bx+POINT.x]	
	movzx	ax, byte[fs:bx+POINT.y]	
	call	SetRabbit					;������������ �������
	add		bx, sizeof.POINT			
	loop	@b							;� ������ ����� ��������� ��������
	jmp		_contCommand1				;� ����������� ���������� ������ � ����
;���������� �������
_helpCommand:
	cmp		al, HELP_COMMAND
	jnz		_resetCommand				;���� �� �������, �� ��������� ������� ������
	mov		[printColor], PRINT_COLOR
	cmp		[inGame], IS_MENU			;���������, ������ �����, �� ���� ��� �� ����
	jz		_contHelpCommand			;���� �� ����, � ����������� � ���������
	popad								;����� ��������� �������� � �������������� ����� ��������
	mov		[saveEAX], eax
	mov		[saveEBX], ebx
	mov		[saveECX], ecx
	mov		[saveEDX], edx
	mov		[saveESI], esi
	mov		[saveEDI], edi
	mov		[saveEBP], ebp
	pop		ax
	mov		[saveIP], ax
	pop		ax	
	push	PROGRAM_SEGM
	push	_menuLoop
	pushad
	mov		[inGame], IS_MENU
_contHelpCommand:	
	call	ClearScreen					;�������� �����
	xor		ax, ax	
	mov		si, szHelp
	xor		di, di
	call	print						;������� �������
	jmp		_goOldKeyHandler			;� ������ �� �����������
;������� ������������
_resetCommand:
	cmp		al, RESET_COMMAND			;���� ������� ������������ - ������������� ���������
	jnz		_offCommand					;����� ��������� �� ���������� �������
	mov		al, 0FEh					;����������� ����� ���� �����-������
	out		64h, al
	jmp		$							;����� � �������� ������ ������������
_offCommand:
	cmp		al, OFF_COMMAND
	jnz		_contCommand
	call	powerOff
_contCommand:	
	cmp		al, CONT_COMMAND
	jnz		_goOldKeyHandler
	popad
	mov		eax, [saveEAX] 
	mov		ebx, [saveEBX]  
	mov		ecx, [saveECX]
	mov		edx, [saveEDX] 
	mov		esi, [saveESI]
	mov		edi, [saveEDI]
	mov		ebp, [saveEBP]	
	add		sp, 2	
	push	[saveIP]
	pushad
	cmp		[isGame], 0
	jz		_gameCommand1
_contCommand1:
	mov		[printColor], HDRGAMECL
	call	ClearScreen
	mov		cx, 80
	xor		di, di
	xor		ax, ax
	mov		al, ' '
	mov		ah, HDRGAMECL
rep	stosw
	mov		di, 80-19
	mov		si, szTail
	xor		ax, ax
	call	print
	mov		ah, FIELDBG
	mov		al, ' '
	mov		cx, 80*24
	mov		di, 160
rep	stosw	
	mov		ah, FIELDBG or BOUNDCL
	mov		al, '#'
	mov		di, 80*2
	mov		cx, 80
rep	stosw
	mov		di, 80*48
	mov		cx, 80
rep	stosw
	mov		di, 80*4
	mov		cx, 22
@@:
	mov		[es:di], ax
	add		di, 80*2
	loop	@b
	mov		cx, 22
	mov		di, 80*4+79*2
@@:
	mov		[es:di], ax
	add		di, 80*2
	loop	@b
	mov		[inGame], 0FFh
_goOldKeyHandler:	
	mov		al, [keyState]
	out		61h, al
	mov		al, 20h
	out		20h, al
	popad	
	iret
	;jmp		[OldKeyHandler]

;GetPosContent - return char in position
;bx - x pos
;ax - y pos
;output
;al - code char
GetPosContent:	
	push	cx
	push	di
	mov		cx, 80*2
	mul		cx
	shl		bx, 1
	add		bx, 80*4+2
	add		ax, bx
	mov		di, ax
	mov		al, [es:di]
	pop		di
	pop		cx
	ret
;GetPrevPos - get previus position piton symbol
;ax - y pos
;bx - x pos
;save getted value in prev_pos
GetPrevPos:
	push	cx
	mov		cx, 80*2
	mul		cx	
	shl		bx, 1
	add		ax, bx
	add		ax, 80*4+2
	
	mov		[prev_pos], ax
	pop		cx
	ret
;SetRabbit - set rabbit in position
;ax - y pos
;bp - x pos
;output
SetRabbit:
	pusha
	mov		cx, 80*2
	mul		cx	
	shl		bp, 1
	add		ax, bp
	add		ax, 80*4+2
	mov		di, ax
	mov		al, '&'
	mov		ah, FIELDBG or RABBITCL
	mov		word[es:di], ax
	popa	
	ret
;UnSetRabbit - set rabbit in position
;ax - y pos
;bp - x pos
;output
UnSetRabbit:
	pusha
	mov		cx, 80*2
	mul		cx	
	shl		bp, 1
	add		ax, bp
	add		ax, 80*4+2
	mov		di, ax
	mov		al, ' '
	mov		ah, FIELDBG
	mov		word[es:di], ax
	popa	
	ret
;KillRabbit - kill rabbit 
;ah - y pos
;al - x pos
;output
KillRabbit:
	pusha
	mov		cx, RABBIT_NUM
	xor		si, si
@@:
	cmp	 	al, [fs:si]
	jne		_next_rabbit_cmp
	cmp		ah, [fs:si+1]
	jne		_next_rabbit_cmp
	mov		[fs:si], byte KILLED_RABBIT_SIGNATURA_X
	inc		[pit_length]	
	add		[trail_info], 2
	push	ax
	push	si	
	mov		ax, [pit_length]
	mov		di, szTail+6
	mov		si, 4
	call	ui2a
	pop		si
	pop		ax
_next_rabbit_cmp:
	add		si, 2
	loop	@b
	popa
	ret
;MovePiton - moved piton
;input:
;al - direction
;
mp_tmp	dd 0, 0
MovePiton:
	pusha
	push	ax
	mov		bx, [pit_pos_x]	
	mov		ax, [pit_pos_y]	
	call	GetPrevPos
	pop		ax
	cmp		al, RIGHT_ARROW
	jnz		_moveLeft			
	
	inc		[pit_pos_x]			
	jmp		_change_pos
_moveLeft:
	cmp		al, LEFT_ARROW
	jnz		_moveUp		
	dec		[pit_pos_x]		
	jmp		_change_pos
_moveUp:
	cmp		al, UP_ARROW
	jnz		_moveDown	
	dec		[pit_pos_y]	
	jmp		_change_pos
_moveDown:	
	cmp		al, DOWN_ARROW
	jnz		_endMovePiton	
	call	GetPosContent	
	inc		[pit_pos_y]		
_change_pos:
	movzx	ax, byte[pit_pos_y]
	movzx	bx, byte[pit_pos_x]
	cmp		ax, MAX_Y
	jna		_checX
	mov		bx, 1
	call	GameOver
_checX:
	cmp		bx, MAX_X
	jna		_checContent
	mov		bx, 1
	call	GameOver
_checContent:
	call	GetPosContent
	cmp		al, '@'
	jnz		_checkRabbit
	xor		bx, bx
	call	GameOver

_checkRabbit:	 	
	mov		bp, 0	
	cmp		al, '&'
	jnz		_nonKillRubbit
	mov		al, byte[pit_pos_x]
	mov		ah, byte[pit_pos_y]
		
	call	KillRabbit		
	mov		bp, 1
_nonKillRubbit:
	mov		ax, [pit_pos_y]	
	mov		bx, 80*2
	mul		bx
	mov		bx, [pit_pos_x]
	add		bx, 161
	shl		bx, 1
	add		ax, bx
	mov		di, ax
	mov		ah, FIELDBG or PITCL
	mov		al, 2	
	mov		[es:di], ax
	;mov		word[es:prev_pos], 767h
	mov		cx, [pit_length]
	test	cx, cx
	jz		_endLengthLoop
	;dec		cx
	mov		di, [prev_pos]
	xor		si, si		
	;mov		
@@:
	;mov		[prev_pos], di
	mov		bx, [gs:si]
	mov		[prev_pos], bx
	;mov		word[di], 0	
	mov		word[es:di],  ((FIELDBG or PITCL) shl 8) or 40h
	mov		[gs:si], di
	add		si, 2
	mov		di, [prev_pos]	
	loop	@b
	;mov		word[es:di], 740h
	;mov		[gs:si], di
_endLengthLoop:
	;mov		bx, [end_tail]
	;cmp		bx, 0FFFFh
	;jz		@f
	test	bp, bp
	jnz		@f
	mov		al, ' '
	;add		bx, [pit_length]
	mov		di, [prev_pos]
	mov		word[es:di], ax
@@:
	
_endMovePiton:
	popa
	ret
;BornRabbit - born rabbit
;input - noting
;output - noting
BornRabbit:
	pusha	
	mov		bx, MAX_X
	mov		si, MAX_Y
	mov		cx, RABBIT_NUM
	xor		di, di
	rdtsc                                                                                                                                                                     
@@:
	
	cmp		byte[fs:di], KILLED_RABBIT_SIGNATURA_X
	jnz		_newBornLoop
	push	word[fs:di]
	push	ax
	rdtsc	
	pop		bp
	add		ax, bp
	xor		dx, dx 	
	div		bx	
	mov		[fs:di], dl
	xor		dx, dx
	push	ax
	rdtsc	
	pop		bp
	add		ax, bp 
	xor		dx, dx	
	div		si	
	mov		[fs:di+1], dl
	movzx	ax, byte[fs:di+1]
	movzx	bx, byte[fs:di]
	call	GetPosContent
	cmp		al, ' '
	jne		_retValuePos
	pop		ax	
	jmp		_newBornLoop
_retValuePos:
	pop		word [fs:di]
_newBornLoop:			
	add		di, 2
	loop	@b
	popa
	ret
;ClearScreen - Clear Screen
;input: nothing
;output: nothing
ClearScreen:
	push	es
	pusha
	mov		ax, 3
	int		10h
	mov		ax, 100h
	mov		ch, 28h
	xor		cl, cl
	int		10h
	mov		ax, PROGRAM_SEGM
	mov		es, ax
	mov		bp, font
	mov		cx, 128
	mov		dx, cx
	mov		bh, 16
	xor		bl, bl
	mov		ax, 1100h
	int		10h
	popa
	pop		es
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                            dw2ah, w2ah, b2ah
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; b2ah - convert word to hex ansii string
; w2ah - convert word to hex ansii string
; dw2ah - convert dword to hex ansii string
;eax - date for convertion
;si - buffer fo string (size of data )
dw2ah1:		
	mov		bx, 7
	jmp		@startConv1
w2ah1:	
	mov		bx, 3
	jmp		@startConv1
b2ah1:	
	mov		bx, 1
@startConv1:
	add		bx, si	
	pusha		
	mov		edx, eax
@convert1:		
	mov		al, dl
	and		al, 0Fh
	cmp		al, 10
	sbb		al, 69h
	das
@to_buf1:
	mov		[bx], al
	shr		edx, 4
	dec		bx
	cmp		bx, si
	jge		@convert	
	popa
	ret
;GameOver - end of game
;input: bx - call case
GameOver:	
	mov		si, szGameOver
	xor		ax, ax
	xor		di, di
	call	print
	mov		di, ax
	xor		ax, ax
	cmp		bx, 1
	jnz		_tailDeth	
	mov		si, szBoundDeth
	jmp		_retGameOver
_tailDeth:
	mov		si, szTailDeth
_retGameOver:
	call	print
	mov		[isGame], 0
	jmp		$
;print - print string
;input:
;si - string buffer
;ax - screen string
;di - position
;out  - nothing
print_tmp dw 0
print:	
	pusha	
	mov		cx, 80*2*25
	mov		bx, 160
	mul		bx
	shl		di, 1
	add		ax, di
	sub		cx, ax
	mov		di, ax
	push	di
	mov		ah, [printColor]
	mov		dx, 160
@@:
	lodsb
	test	al, al	
	jz		_retPrint
	cmp		al, 10
	jnz		_outChar
	add		di, dx
	mov		dx, 160
	jmp		_newPrnLoop
_outChar:
	stosw
	sub		dx, 2
	jnz		_newPrnLoop
	mov		dx, 160
_newPrnLoop:
	loop	@b
_retPrint:
	pop		ax
	sub		di, ax
	shr		di, 1
	mov		[print_tmp], di
	popa
	mov		ax, [print_tmp]
	ret
;
PrintMenu:
	push	si
	push	ax
	push	di
	call	ClearScreen
	mov		si, szMenu
	xor		ax, ax
	xor		di, di
	call	print
	pop		di
	pop		ax
	pop		si
	ret
powerOff:  
	mov 	ax, 5301h  
	xor		bx, bx  
	int 	15h  
	mov 	ax, 5308h  
	mov 	bx, 1  
	mov 	cx, bx  
	int 	15h  
	mov 	ax, 530Dh  
	mov 	bx, 1  
	mov 	cx, bx  
	int 	15h  
	mov 	ax, 530Fh  
	mov 	bx, 1  
	mov 	cx, bx  
	int 	15h  
	mov 	ax, 530Eh  
	xor 	bx, bx  
	mov 	cx, 102h  
	int 	15h  
	mov 	ax, 5307h  
	mov 	bx, 1  
	mov 	cx, 3  
	int 	15h  
	jmp 	$
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                              ui2a
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;convert integer to string
;input:
;di - string
;ax - integer
;dx: 0 - lowcase; 1 - uppercase
;si - width
;output:
;eax - number converted digit with sign
ui2a_buff 	equ	word [bp-2]
ui2a_num 	equ	word [bp-4]
ui2a_bcd	equ	dword[bp-8]
ui2a_case	equ	word[bp-10]
ui2a_width 	equ word[bp-12]
ui2a:	
	push	bp
	mov		bp, sp
	sub		sp, 14
	pusha
	mov		bx, 10
	mov		ui2a_case, dx
	mov		ui2a_buff, di
	mov		ui2a_width, si	
	mov		ui2a_bcd, 0
	lea		si, ui2a_bcd
	
@@:
	xor		dx, dx
	div		bx
	mov		[ss:si], dl
	inc		si	
	test	ax, ax
	jne		@b
	lea		cx, ui2a_bcd
	sub		si, cx
	cmp		si, ui2a_width
	jnb		_ui2a_cont
	mov		si, ui2a_width
_ui2a_cont:
	xchg	si, cx
	add		di, cx
	mov		ui2a_num, di
	;mov		[di], byte 0
	dec		di	
	mov		dx, ui2a_case
	test	dx, dx
	jz		@f
	mov		dl, 'A'
	jmp		_ui2a_outloop	
@@:
	mov		dl, 'a'
_ui2a_outloop:
	mov		al, [ss:si]
	cmp		al, 9
	ja		_dalph
	add		al, '0'
	jmp		_ui2a_conout
_dalph:
	sub		al, 10
	add		al, dl
_ui2a_conout:
	mov		[di], al	
	dec		di
	inc		si
	loop	_ui2a_outloop
ui2a_ret:		
	popa
	mov		ax, ui2a_num
	sub		ax, ui2a_buff	
	mov		sp, bp
	pop		bp
	ret
isGame		db 0
inGame		db 0
saveEAX		dd 0
saveEBX		dd 0
saveECX		dd 0
saveEDX		dd 0
saveEDI		dd 0
saveESI		dd 0
saveEBP		dd 0
saveIP		dw _mainLoop
printColor  db 	7
szGameOver db ' ����� ����. ',0	
pit_pos_x	dw 0
pit_pos_y	dw 0
rabbit_tmp_pos 	POINT
OldKeyHandler dd 0
prev_pos	dw 0
pit_length dw 0
trail_info dw 0
savedSec db 0
direction db RIGHT_ARROW
tick	db	0
keyState db 0
time	dw	0
szTime	db '0','0', ':', '0','0', ':', '0','0', 0
szBoundDeth db '����� �������� �� �������������� �����������', 0
szTailDeth db '����� ���� ��� ����', 0
szTail  db '�����:'
		db '0000', 0
szMenu 		 db '�����', 10 
 			 db '������� �������, ��������������� ���������� ���� ������', 10 
 			 db '��� ������ ������� F1-F3 ��� �������� � ���� ������� Esc', 10
 			 db 'F1. �������� ������� �� ����', 10 
			 db 'F2. ����� ����', 10
 			 db 'F3. ���������� ����. ��� ���������� - ����� ����', 10
 			 db 'F4. ����������� ���������', 10 
			 db 'F5. ��������� ���������', 10, 0
szHelp:		 db '���� ����� - Snake (������, ����, ������ � �.�.) - �������� � ��������, ��� � ����� 1970-� �����. � ���������� ������� �������� ���������� ������ ���� ��� ��������� ���������� � ����������� �������� - MS-DOS, Windows, Mac OS, Linux, ������� ���������, ������� ��������� � �.�., ��� � ����������, ��� � � ��������� ��������.', 10
			 db '������� ���� ����������� � ���������� ������������ ���������, �������������� �� �������� ���� � ���������� �� ��� ����.' 
			 db '��� �������� ���� �������� ������������� � �����. � ������ �������� ���� ����� ������ ��������� �� �������� ���� �������.'
			 db  ' ������ - ��������� ��� �������� ��� ����� ����� �������. ��������� �������� ����� ��������� ������� �� ��������������� ������� �� ��������� �� �������������� ����������.'
			 db  ' ���������� �������� �������� ������. �������� �� ����� ����������� ����� ������� �� ����������� �����, � ��� �� �� �����. � ���� ������ ���� �������������. �� ���� ���������� ����� ��������� ��������� ����������� �� ������� � �������', 10
			 db '������� ����������:', 10
			 db 1Ah, ' - ��������� ������ ������', 10
			 db 1Bh, ' - ��������� ������ �����', 10
			 db 18h, ' - ��������� ������ �����', 10
			 db 19h, ' - ��������� ������ ����', 10
			 db 'Esc - ����� ����', 10
			 db 'F1 - ����� �������', 10
			 db 'F2 - ����� ����', 10
			 db 'F3 - ����������� ����, ���� ���� �� ���������� - ����� ����', 10
			 db 'F4 - ����������� ���������', 10
			 db 'F5 - ��������� ���������', 0			 
			 
			
font:
include	'font8x16.inc'
align	512
@end_prog:
db 1474560-@end_prog-512 dup 0

